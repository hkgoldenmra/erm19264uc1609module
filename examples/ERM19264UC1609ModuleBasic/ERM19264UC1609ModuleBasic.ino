#include <ERM19264UC1609Module.h>

const byte kPin = 10;
const byte aPin = 9;
const byte gndPin = 8;
const byte vddPin = 7;
const byte sckPin = 6;
const byte sdaPin = 5;
const byte rstPin = 4;
const byte cdPin = 3;
const byte csPin = 2;
ERM19264UC1609Module module = ERM19264UC1609Module(rstPin, cdPin, csPin, sdaPin, sckPin);

void setup() {
	pinMode(kPin, OUTPUT);
	pinMode(aPin, OUTPUT);
	pinMode(gndPin, OUTPUT);
	pinMode(vddPin, OUTPUT);
	digitalWrite(kPin, LOW);
	digitalWrite(aPin, HIGH);
	digitalWrite(gndPin, LOW);
	digitalWrite(vddPin, HIGH);
	module.initial(192, 8);
	module.setTemperatureCompensation(ERM19264UC1609Module::TC_000);
	module.setPowerControl(ERM19264UC1609Module::PC_EXTERNAL, ERM19264UC1609Module::PC_06MA);
	module.setMappingControl(false, true);
	module.setContrast(0x10);
	module.clear();
	module.setDisplay(true);
}

void loop() {
	testFont();
}

void testFont() {
	module.clear();
	module.setInverseDisplay(false);
	module.drawText(0, 0, String("#include <stdio.h>\r\nvoid main() {\r\n\tprintf(\"UC1609\\n\");\r\n}"), ASCII_UNIFONT_NAME, true);
	delay(2000);
	module.clear();
	module.setInverseDisplay(true);
	module.drawText(0, 0, String("import java.lang.*;\r\npublic class Test {\r\n\tpublic static void main(String[] args) {\r\n\t\tSystem.out.println(\"UC1609\");\r\n\t}\r\n}"), ASCII_8X6_NAME, true);
	delay(2000);
}

void testContrast() {
	for (byte c = 0; c < 64; c++) {
		char hexString[3];
		char decString[4];
		String binString = "";
		sprintf(hexString, "%02X", c);
		sprintf(decString, "%03d", c);
		for (byte i = 0; i < 8; i++) {
			binString = ((c & (1 << i)) ? "1" : "0") + binString;
		}
		module.setContrast(c);
		String name = ASCII_UNIFONT_NAME;
		module.drawText(0, 0, String("0x"), name, false);
		module.drawText(2, 0, hexString, name, false);
		module.drawText(0, 1, String("B"), name, false);
		module.drawText(1, 1, binString, name, false);
		module.drawText(0, 2, decString, name, false);
		delay(1000);
	}
}

void testCredit() {
	const byte data[][32] = {
		{0x00, 0x00, 0x00, 0xFC, 0x54, 0x56, 0x55, 0x54, 0x54, 0x54, 0x54, 0xFC, 0x00, 0x80, 0x40, 0x00, 0x40, 0x42, 0x42, 0x23, 0x22, 0x22, 0x12, 0x12, 0x0A, 0x46, 0x82, 0x7F, 0x01, 0x00, 0x00, 0x00,}, /* 身/身 */
		{0x00, 0x00, 0x00, 0x7F, 0x49, 0x49, 0x49, 0x49, 0x49, 0x49, 0x49, 0x7F, 0x00, 0x00, 0x00, 0x00, 0x81, 0x41, 0x21, 0x1D, 0x21, 0x41, 0x81, 0xFF, 0x89, 0x89, 0x89, 0x89, 0x89, 0x81, 0x81, 0x00,}, /* 日/是 */
		{0x04, 0x04, 0x24, 0x24, 0x6F, 0xA4, 0x24, 0x34, 0x24, 0xA4, 0x6F, 0x24, 0x24, 0x04, 0x04, 0x00, 0x01, 0x01, 0x01, 0xF9, 0x49, 0x49, 0x49, 0x49, 0x49, 0x49, 0x49, 0xF9, 0x01, 0x01, 0x01, 0x00,}, /* 艸/菩 */
		{0x10, 0x10, 0x10, 0xFF, 0x10, 0x90, 0x00, 0xFE, 0x92, 0x92, 0x92, 0x92, 0x92, 0xFE, 0x00, 0x00, 0x04, 0x44, 0x82, 0x7F, 0x01, 0x80, 0x42, 0x3A, 0x42, 0x82, 0xFE, 0x92, 0x92, 0x92, 0x82, 0x00,}, /* 手/提 */
		{0x10, 0x90, 0xFF, 0x90, 0x14, 0xA4, 0xA4, 0xBF, 0xA4, 0xA4, 0x94, 0x10, 0x10, 0xFF, 0x10, 0x00, 0x06, 0x01, 0xFF, 0x00, 0x41, 0xCF, 0x54, 0x44, 0x34, 0x2F, 0x20, 0x43, 0x80, 0x7F, 0x00, 0x00,}, /* 木/樹 */
		{0x00, 0x00, 0x80, 0x00, 0x00, 0xE0, 0x02, 0x04, 0x18, 0x00, 0x00, 0x00, 0x40, 0x80, 0x00, 0x00, 0x10, 0x0C, 0x03, 0x00, 0x00, 0x3F, 0x40, 0x40, 0x40, 0x40, 0x40, 0x78, 0x00, 0x01, 0x0E, 0x00,}, /* 心/心 */
		{0x10, 0x10, 0xF0, 0x1F, 0x10, 0x10, 0xF0, 0x00, 0x00, 0xF8, 0x08, 0x08, 0x08, 0xF8, 0x00, 0x00, 0x80, 0x41, 0x22, 0x14, 0x08, 0x16, 0x61, 0x00, 0x00, 0x7F, 0x20, 0x20, 0x20, 0x7F, 0x00, 0x00,}, /* 女/如 */
		{0x00, 0xFC, 0x44, 0x44, 0x44, 0xFC, 0x00, 0x00, 0xFE, 0x22, 0x22, 0x22, 0x22, 0xFE, 0x00, 0x00, 0x00, 0x0F, 0x04, 0x04, 0x04, 0x8F, 0x40, 0x30, 0x0F, 0x02, 0x02, 0x42, 0x82, 0x7F, 0x00, 0x00,}, /* 日/明 */
		{0x20, 0x10, 0x2C, 0xE3, 0x24, 0x08, 0x20, 0xA4, 0xAC, 0xB5, 0xA6, 0xB4, 0xAC, 0xA4, 0x20, 0x00, 0x45, 0xD9, 0x41, 0x3F, 0x29, 0xA5, 0x80, 0x4F, 0x3A, 0x0A, 0x0A, 0x7A, 0x8A, 0x8F, 0xE0, 0x00,}, /* 金/鏡 */
		{0x10, 0x90, 0xFF, 0x50, 0x92, 0x8A, 0xEA, 0xAA, 0xAA, 0xAF, 0xAA, 0xAA, 0xEA, 0x8A, 0x82, 0x00, 0x06, 0x01, 0xFF, 0x00, 0x81, 0xA2, 0xAA, 0xAE, 0xAA, 0xFA, 0xAA, 0xAE, 0xAA, 0xA2, 0x81, 0x00,}, /* 木/檯 */
		{0x00, 0xFC, 0x84, 0x84, 0x84, 0xFC, 0x40, 0x48, 0x48, 0x48, 0x7F, 0x48, 0xC8, 0x48, 0x40, 0x00, 0x00, 0x3F, 0x10, 0x10, 0x10, 0x3F, 0x00, 0x02, 0x0A, 0x12, 0x42, 0x82, 0x7F, 0x02, 0x02, 0x00,}, /* 日/時 */
		{0x00, 0xFC, 0x84, 0x84, 0x84, 0xFC, 0x40, 0x48, 0x48, 0x48, 0x7F, 0x48, 0xC8, 0x48, 0x40, 0x00, 0x00, 0x3F, 0x10, 0x10, 0x10, 0x3F, 0x00, 0x02, 0x0A, 0x12, 0x42, 0x82, 0x7F, 0x02, 0x02, 0x00,}, /* 日/時 */
		{0x04, 0xC4, 0x5F, 0x54, 0xF4, 0x54, 0x5F, 0xC4, 0x14, 0x10, 0xFF, 0x10, 0x10, 0xF0, 0x00, 0x00, 0x80, 0x95, 0x95, 0x95, 0x7F, 0x55, 0xD5, 0x55, 0x20, 0x18, 0x47, 0x80, 0x40, 0x3F, 0x00, 0x00,}, /* 力/勤 */
		{0x10, 0x10, 0x10, 0xFF, 0x10, 0x90, 0xC8, 0x48, 0xFF, 0x48, 0x48, 0xFF, 0x48, 0x78, 0x00, 0x00, 0x04, 0x44, 0x82, 0x7F, 0x01, 0x80, 0x43, 0x32, 0x0F, 0x02, 0x02, 0xFF, 0x12, 0x22, 0x1E, 0x00,}, /* 手/拂 */
		{0x10, 0x10, 0x10, 0xFF, 0x90, 0x40, 0x90, 0x90, 0x90, 0x10, 0xFF, 0x10, 0x11, 0x16, 0x10, 0x00, 0x02, 0x42, 0x81, 0x7F, 0x00, 0x20, 0x60, 0x3F, 0x10, 0x10, 0x01, 0x0E, 0x30, 0x40, 0xF0, 0x00,}, /* 手/拭 */
		{0x04, 0x04, 0x04, 0xE4, 0xAF, 0xA4, 0xA4, 0xA4, 0xA4, 0xA4, 0xAF, 0xE4, 0x04, 0x04, 0x04, 0x00, 0x80, 0x88, 0x88, 0x4B, 0x4A, 0x2A, 0x1A, 0x0E, 0x1A, 0x2A, 0x4A, 0x4B, 0x88, 0x88, 0x80, 0x00,}, /* 艸/莫 */
		{0x80, 0x60, 0xF8, 0x07, 0x04, 0xE4, 0x24, 0x24, 0x24, 0xFF, 0x24, 0x24, 0x24, 0xE4, 0x04, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x80, 0x81, 0x45, 0x29, 0x11, 0x2F, 0x41, 0x41, 0x81, 0x81, 0x80, 0x00,}, /* 人/使 */
		{0x10, 0x12, 0x92, 0x52, 0xF7, 0x52, 0x5A, 0x52, 0x52, 0x52, 0x57, 0xD2, 0x12, 0x12, 0x10, 0x00, 0x41, 0x31, 0x00, 0x00, 0x3B, 0x42, 0x42, 0x46, 0x5A, 0x42, 0x42, 0x73, 0x00, 0x08, 0x30, 0x00,}, /* 心/惹 */
		{0x00, 0x00, 0xFC, 0x54, 0xD4, 0x54, 0x7C, 0x55, 0x56, 0xD4, 0x7C, 0x54, 0x54, 0x74, 0x04, 0x00, 0x80, 0x60, 0x5F, 0x40, 0x57, 0x55, 0x55, 0x55, 0x78, 0x53, 0x55, 0x55, 0x55, 0x44, 0x46, 0x00,}, /* 土/塵 */
		{0x20, 0x20, 0xFF, 0x20, 0x20, 0x10, 0x98, 0xF4, 0x92, 0x91, 0x90, 0x94, 0x98, 0x30, 0x00, 0x00, 0x10, 0x30, 0x1F, 0x08, 0x88, 0x85, 0x44, 0x24, 0x14, 0x0F, 0x14, 0x24, 0x44, 0x84, 0x84, 0x00,}, /* 土/埃 */
		{0x04, 0x04, 0x24, 0x24, 0x6F, 0xA4, 0x24, 0x34, 0x24, 0xA4, 0x6F, 0x24, 0x24, 0x04, 0x04, 0x00, 0x01, 0x01, 0x01, 0xF9, 0x49, 0x49, 0x49, 0x49, 0x49, 0x49, 0x49, 0xF9, 0x01, 0x01, 0x01, 0x00,}, /* 艸/菩 */
		{0x10, 0x10, 0x10, 0xFF, 0x10, 0x90, 0x00, 0xFE, 0x92, 0x92, 0x92, 0x92, 0x92, 0xFE, 0x00, 0x00, 0x04, 0x44, 0x82, 0x7F, 0x01, 0x80, 0x42, 0x3A, 0x42, 0x82, 0xFE, 0x92, 0x92, 0x92, 0x82, 0x00,}, /* 手/提 */
		{0x00, 0x10, 0x10, 0x10, 0x10, 0xD0, 0x30, 0xFF, 0x30, 0xD0, 0x10, 0x10, 0x10, 0x10, 0x00, 0x00, 0x10, 0x08, 0x04, 0x02, 0x09, 0x08, 0x08, 0xFF, 0x08, 0x08, 0x09, 0x02, 0x04, 0x08, 0x10, 0x00,}, /* 木/本 */
		{0x00, 0x50, 0x48, 0x47, 0xFC, 0x44, 0xFC, 0x44, 0xFC, 0x44, 0xFC, 0x44, 0x44, 0x44, 0x00, 0x00, 0x80, 0x44, 0x34, 0x04, 0x07, 0x14, 0x67, 0x04, 0x17, 0x64, 0x07, 0x04, 0x14, 0xE4, 0x00, 0x00,}, /* 火/無 */
		{0x10, 0x90, 0xFF, 0x90, 0x14, 0xA4, 0xA4, 0xBF, 0xA4, 0xA4, 0x94, 0x10, 0x10, 0xFF, 0x10, 0x00, 0x06, 0x01, 0xFF, 0x00, 0x41, 0xCF, 0x54, 0x44, 0x34, 0x2F, 0x20, 0x43, 0x80, 0x7F, 0x00, 0x00,}, /* 木/樹 */
		{0x00, 0xFC, 0x44, 0x44, 0x44, 0xFC, 0x00, 0x00, 0xFE, 0x22, 0x22, 0x22, 0x22, 0xFE, 0x00, 0x00, 0x00, 0x0F, 0x04, 0x04, 0x04, 0x8F, 0x40, 0x30, 0x0F, 0x02, 0x02, 0x42, 0x82, 0x7F, 0x00, 0x00,}, /* 日/明 */
		{0x20, 0x10, 0x2C, 0xE3, 0x24, 0x08, 0x20, 0xA4, 0xAC, 0xB5, 0xA6, 0xB4, 0xAC, 0xA4, 0x20, 0x00, 0x45, 0xD9, 0x41, 0x3F, 0x29, 0xA5, 0x80, 0x4F, 0x3A, 0x0A, 0x0A, 0x7A, 0x8A, 0x8F, 0xE0, 0x00,}, /* 金/鏡 */
		{0x08, 0x08, 0xC8, 0x08, 0x08, 0xF8, 0x09, 0x0E, 0x08, 0xF8, 0x08, 0x48, 0x88, 0x08, 0x08, 0x00, 0x08, 0x86, 0x41, 0x20, 0x18, 0x07, 0x00, 0x40, 0x80, 0x7F, 0x00, 0x00, 0x01, 0x0E, 0x00, 0x00,}, /* 亠/亦 */
		{0x08, 0x88, 0x88, 0x88, 0x88, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x88, 0x88, 0x88, 0x88, 0x08, 0x00, 0x10, 0x10, 0x10, 0x10, 0x10, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x10, 0x10, 0x10, 0x10, 0x10, 0x00,}, /* 非/非 */
		{0x10, 0x90, 0xFF, 0x50, 0x92, 0x8A, 0xEA, 0xAA, 0xAA, 0xAF, 0xAA, 0xAA, 0xEA, 0x8A, 0x82, 0x00, 0x06, 0x01, 0xFF, 0x00, 0x81, 0xA2, 0xAA, 0xAE, 0xAA, 0xFA, 0xAA, 0xAE, 0xAA, 0xA2, 0x81, 0x00,}, /* 木/檯 */
		{0x00, 0x10, 0x10, 0x10, 0x10, 0xD0, 0x30, 0xFF, 0x30, 0xD0, 0x10, 0x10, 0x10, 0x10, 0x00, 0x00, 0x10, 0x08, 0x04, 0x02, 0x09, 0x08, 0x08, 0xFF, 0x08, 0x08, 0x09, 0x02, 0x04, 0x08, 0x10, 0x00,}, /* 木/本 */
		{0x00, 0x08, 0x88, 0x68, 0x88, 0x08, 0x08, 0xFF, 0x08, 0x08, 0x88, 0x68, 0x88, 0x08, 0x00, 0x00, 0x20, 0x21, 0x10, 0x10, 0x08, 0x05, 0x02, 0xFF, 0x02, 0x05, 0x08, 0x10, 0x10, 0x21, 0x20, 0x00,}, /* 人/來 */
		{0x00, 0x50, 0x48, 0x47, 0xFC, 0x44, 0xFC, 0x44, 0xFC, 0x44, 0xFC, 0x44, 0x44, 0x44, 0x00, 0x00, 0x80, 0x44, 0x34, 0x04, 0x07, 0x14, 0x67, 0x04, 0x17, 0x64, 0x07, 0x04, 0x14, 0xE4, 0x00, 0x00,}, /* 火/無 */
		{0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,}, /* 一/一 */
		{0x40, 0x3C, 0x10, 0xFF, 0x10, 0x10, 0x20, 0x10, 0x8F, 0x78, 0x08, 0xF8, 0x08, 0xF8, 0x00, 0x00, 0x02, 0x06, 0x02, 0xFF, 0x01, 0x01, 0x04, 0x42, 0x21, 0x18, 0x46, 0x81, 0x40, 0x3F, 0x00, 0x00,}, /* 牛/物 */
		{0x80, 0x40, 0x30, 0xEC, 0x03, 0x04, 0xE4, 0x24, 0x24, 0xE4, 0x04, 0x04, 0xFC, 0x04, 0x04, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x0F, 0x04, 0x04, 0x0F, 0x40, 0x80, 0x7F, 0x00, 0x00, 0x00,}, /* 人/何 */
		{0x00, 0x00, 0xF8, 0x08, 0x48, 0x48, 0x48, 0x7F, 0xAA, 0xAA, 0xAA, 0xAA, 0x8A, 0xC8, 0x18, 0x00, 0x80, 0x60, 0x1F, 0x90, 0x88, 0x57, 0x24, 0x54, 0x4C, 0xA0, 0x9E, 0x82, 0x9E, 0xA0, 0xB8, 0x00,}, /* 虍/處 */
		{0x10, 0x12, 0x92, 0x52, 0xF7, 0x52, 0x5A, 0x52, 0x52, 0x52, 0x57, 0xD2, 0x12, 0x12, 0x10, 0x00, 0x41, 0x31, 0x00, 0x00, 0x3B, 0x42, 0x42, 0x46, 0x5A, 0x42, 0x42, 0x73, 0x00, 0x08, 0x30, 0x00,}, /* 心/惹 */
		{0x00, 0x00, 0xFC, 0x54, 0xD4, 0x54, 0x7C, 0x55, 0x56, 0xD4, 0x7C, 0x54, 0x54, 0x74, 0x04, 0x00, 0x80, 0x60, 0x5F, 0x40, 0x57, 0x55, 0x55, 0x55, 0x78, 0x53, 0x55, 0x55, 0x55, 0x44, 0x46, 0x00,}, /* 土/塵 */
		{0x20, 0x20, 0xFF, 0x20, 0x20, 0x10, 0x98, 0xF4, 0x92, 0x91, 0x90, 0x94, 0x98, 0x30, 0x00, 0x00, 0x10, 0x30, 0x1F, 0x08, 0x88, 0x85, 0x44, 0x24, 0x14, 0x0F, 0x14, 0x24, 0x44, 0x84, 0x84, 0x00,}, /* 土/埃 */
	};
	byte w = 16;
	byte h = 16;
	for (byte l = 0 ; l < 4; l++){
		for (byte c = 0; c < 5; c++){
			module.drawImage(c * w, l * h, w, h, data[l * 5 + c]);
		}
		module.scrollLineUp(16);
		delay(1000);
	}
	for (byte l = 0 ; l < 4; l++){
		for (byte c = 0; c < 5; c++){
			module.drawImage(c * w, l * h, w, h, data[l * 5 + c + 20]);
		}
		module.scrollLineUp(16);
		delay(1000);
	}
}
#ifndef ERM19264UC1609MODULE_H
#define ERM19264UC1609MODULE_H

#include <Arduino.h>
#include <ASCIIUnifont.h>
#include <ASCII8x6.h>
#include <ChineseUnifont.h>

class ERM19264UC1609Module {
	private:
		static const byte SPACE_COUNT = 2;
		static const unsigned int DELAY_MICROSECONDS = 1;
		byte rstPin;
		byte cdPin;
		byte csPin;
		byte sdaPin;
		byte sckPin;
		byte columns = 192;
		byte pages = 64;
		byte cursorColumn = 0;
		byte cursorPage = 0;
		byte scrollLine = 0;
		bool isDefaultPins();
		void setPin(byte, bool);
		void transferBit(bool);
		void transferByte(byte);
		void transfer(byte);
		void sendControl(byte, byte);
	public:
		//
		static const byte TC_000 = 0;
		static const byte TC_005 = 1;
		static const byte TC_010 = 2;
		static const byte TC_015 = 3;
		//
		static const bool PC_EXTERNAL = false;
		static const bool PC_INTERNAL = true;
		static const byte PC_06MA = 0;
		static const byte PC_10MA = 1;
		static const byte PC_14MA = 2;
		static const byte PC_23MA = 3;
		//
		static const bool MC_LEFT = false;
		static const bool MC_RIGHT = true;
		static const bool MC_BOTTOM = false;
		static const bool MC_TOP = true;
		//
		static const byte FR_076FPS = 0;
		static const byte FR_095FPS = 1;
		static const byte FR_132FPS = 2;
		static const byte FR_168FPS = 3;
		//
		ERM19264UC1609Module(byte, byte, byte, byte, byte);
		ERM19264UC1609Module(byte, byte);
		void initial(byte, byte);
		void hardwareReset();
		void sendData(byte);
		void setColumnAddress(byte);
		void setTemperatureCompensation(byte);
		void setPowerControl(bool, byte);
		void setScrollLine(byte);
		void scrollLineUp(byte);
		void scrollLineDown(byte);
		void setPageAddress(byte);
		void setContrast(byte);
		void setPartialDisplayControl(bool);
		void setRAMAddressControl(bool, bool, bool);
		void setFrameRate(byte);
		void setAllPixel(bool);
		void setInverseDisplay(bool);
		void setDisplay(bool);
		void setMappingControl(bool, bool);
		void softwareReset();
		void setRatio(byte);
		void fillPage(byte, byte);
		void fillScreen(byte);
		void clear();
		void fill();
		void drawImage(byte, byte, byte, byte, const byte*);
		void drawText(byte, byte, char, String);
		void drawText(byte, byte, String, String, bool);
		void drawText(byte, byte, char*, String, bool);
};

#endif
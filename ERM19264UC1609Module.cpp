#include <ERM19264UC1609Module.h>

bool ERM19264UC1609Module::isDefaultPins() {
	return !((this->csPin != SS) || (this->sdaPin != MOSI) || (this->sckPin != SCK));
}

void ERM19264UC1609Module::setPin(byte pin, bool data) {
	digitalWrite(pin, data);
	delayMicroseconds(ERM19264UC1609Module::DELAY_MICROSECONDS);
}

void ERM19264UC1609Module::transferBit(bool data) {
	this->setPin(this->sckPin, LOW);
	this->setPin(this->sdaPin, data);
	this->setPin(this->sckPin, HIGH);
}

void ERM19264UC1609Module::transferByte(byte data) {
	for (byte i = 0x80; i > 0; i >>= 1) {
		this->transferBit(data & i);
	}
}

void ERM19264UC1609Module::transfer(byte data) {
	this->setPin(this->csPin, LOW);
	this->transferByte(data);
	this->setPin(this->csPin, HIGH);
}

void ERM19264UC1609Module::sendControl(byte command, byte data) {
	this->setPin(this->cdPin, LOW);
	this->transfer(command | data);
}

/**
 * Construct a ERM19264UC1609Module object with user defined SPI pins.
 * @param rstPin Reset (RST) pin.
 * @param cdPin Control or Command / Data (CD) pin. Sometimes called Instruction / Data (ID) or Register (RS).
 * @param csPin Chip Select (CS) pin. Sometimes called Slave Select (SS).
 * @param sdaPin Serial Data (SDA) pin. Sometimes called Master Out Slave In (MOSI) or Data In (DIN).
 * @param sckPin Serial Clock (SCK) pin. Sometimes use other ABBR called (SCL) or CLOCK (CLK).
 */
ERM19264UC1609Module::ERM19264UC1609Module(byte rstPin, byte cdPin, byte csPin, byte sdaPin, byte sckPin) {
	this->rstPin = rstPin;
	this->cdPin = cdPin;
	this->csPin = csPin;
	this->sdaPin = sdaPin;
	this->sckPin = sckPin;
}

/**
 * Construct a ERM19264UC1609Module object with Arduino predefined SPI pins.
 * @param rstPin Reset (RST) pin.
 * @param cdPin Control or Command / Data (CD) pin. Sometimes called Instruction / Data (ID) or Register (RS).
 */
ERM19264UC1609Module::ERM19264UC1609Module(byte rstPin, byte cdPin)
: ERM19264UC1609Module::ERM19264UC1609Module(rstPin, cdPin, SS, MOSI, SCK) {
}

/**
 * Initial all pins and define LCD columns and pages.
 * @param columns LCD columns.
 * @param pages LCD pages.
 */
void ERM19264UC1609Module::initial(byte columns, byte pages) {
	pinMode(this->rstPin, OUTPUT);
	pinMode(this->cdPin, OUTPUT);
	pinMode(this->csPin, OUTPUT);
	pinMode(this->sdaPin, OUTPUT);
	pinMode(this->sckPin, OUTPUT);
	digitalWrite(this->rstPin, HIGH);
	digitalWrite(this->csPin, HIGH);
	this->columns = columns;
	this->pages = pages;
	this->hardwareReset();
	this->softwareReset();
}

/**
 * Reset the IC settings using RST pin (hardware level).
 */
void ERM19264UC1609Module::hardwareReset() {
	this->setPin(this->rstPin, LOW);
	this->setPin(this->rstPin, HIGH);
}

/**
 * Send a 8-bit data to Data Register.
 * @param data 8-bit data.
 */
void ERM19264UC1609Module::sendData(byte data) {
	this->cursorColumn++;
	if (this->cursorColumn >= this->columns) {
		this->cursorColumn = 0;
		this->cursorPage++;
	}
	if (this->cursorPage >= this->pages) {
		this->cursorPage = 0;
	}
	this->setPin(this->cdPin, HIGH);
	this->transfer(data);
}

/**
 * Set column address.
 * @param data 0 to this->columns - 1.
 */
void ERM19264UC1609Module::setColumnAddress(byte data) {
	this->cursorColumn = (data %= this->columns);
	this->sendControl(0x00, data & 0x0F);
	this->sendControl(0x10, (data & 0xF0) >> 4);
}

/**
 * Set V(BIAS) temperature compensation coefficient.
 * @param data Temperature compensation curve definition: 0 = -0.00%; 1 = -0.05%; 2 = -0.10%; 3 = -0.15%.
 */
void ERM19264UC1609Module::setTemperatureCompensation(byte data) {
	this->sendControl(0x24, data & 0x03);
}

/**
 * Set Low-Pump charge current.
 * @param internal Program the build-in charge pump stages: 0 = External; 1 = Internal (7x charge pump)
 * @param data Temperature compensation curve definition: 0 = 0.6mA; 1 = 1.0mA; 2 = 1.4mA; 3 = 2.3mA.
 */
void ERM19264UC1609Module::setPowerControl(bool internal, byte data) {
	this->sendControl(0x28, (internal << 2) | (data & 0x03));
	// Wait for power on
	delay(10);
}

/**
 * Set scroll line displayed image up by n rows.
 * @param data 0 to this->pages * 8 - 1.
 */
void ERM19264UC1609Module::setScrollLine(byte data) {
	this->sendControl(0x40, this->scrollLine = (data %= this->pages * 8));
}

/**
 * Scroll line increase by n row.
 */
void ERM19264UC1609Module::scrollLineUp(byte data) {
	this->setScrollLine(this->scrollLine += data);
}

/**
 * Scroll line decrease by n row.
 */
void ERM19264UC1609Module::scrollLineDown(byte data) {
	this->setScrollLine(this->scrollLine -= data);
}

/**
 * Set page address.
 * @param data 0 to this->pages - 1.
 */
void ERM19264UC1609Module::setPageAddress(byte data) {
	this->sendControl(0xB0, this->cursorPage = (data %= this->pages));
}

/**
 * Set the contrast level between 0 to 63.
 * @param data between 0 to 63.
 */
void ERM19264UC1609Module::setContrast(byte data) {
	this->sendControl(0x81, 0x00);
	this->sendControl(0x81, (data & 0x3F) << 1);
}

/**
 * Set partial display function.
 * @param data 0 = disable; 1 = enable.
 */
void ERM19264UC1609Module::setPartialDisplayControl(bool data) {
	this->sendControl(0x84, data);
}

void ERM19264UC1609Module::setRAMAddressControl(bool wrap, bool order, bool direction) {
	this->sendControl(0x88, (wrap << 0) | (order << 1) | (direction << 2));
}

/**
 * Set frame rate.
 * @param data 0 = 76 FPS; 1 = 95 FPS; 2 = 132 FPS; 3 = 168 FPS.
 */
void ERM19264UC1609Module::setFrameRate(byte data) {
	this->sendControl(0xA0, data & 0x03);
}

/**
 * Test all pixel of the LCD screen. The test no effect on the existing data stored in display RAM.
 * @param data
 */
void ERM19264UC1609Module::setAllPixel(bool data) {
	this->sendControl(0xA4, data);
}

/**
 * Set inverse display.
 * @param data 0 = standard; 1 = inversed.
 */
void ERM19264UC1609Module::setInverseDisplay(bool data) {
	this->sendControl(0xA6, data);
}

/**
 * Set display mode.
 * @param data 0 = disable; 1 = enable.
 */
void ERM19264UC1609Module::setDisplay(bool data) {
	this->sendControl(0xAE, data);
}

/**
 * Map the control start from.
 * @param mx 0 = start from left; 1 = start from right.
 * @param my 0 = start from bottom; 1 = start from top.
 */
void ERM19264UC1609Module::setMappingControl(bool mx, bool my) {
	this->sendControl(0xC0, (my << 2) | (mx << 1));
}

/**
 * Control Register will be reset to the default values. Data Register will not be affected.
 */
void ERM19264UC1609Module::softwareReset() {
	this->sendControl(0xE2, 0x00);
}

/**
 * Set LCD Ratio.
 * @param data Ratio definition: 0 = 6; 1 = 7; 2 = 8; 3 = 9.
 */
void ERM19264UC1609Module::setRatio(byte data) {
	this->sendControl(0xE8, data & 0x03);
}

/**
 * Fill the page with specific pattern.
 * @param page Target path.
 * @param data Filled pattern.
 */
void ERM19264UC1609Module::fillPage(byte page, byte data) {
	this->setColumnAddress(0);
	this->setPageAddress(page);
	for (byte i = 0; i < this->columns; i++) {
		this->sendData(data);
	}
}

/**
 * Fill the screen with specific pattern.
 * @param data Filled pattern.
 */
void ERM19264UC1609Module::fillScreen(byte data) {
	for (byte i = 0; i < this->pages; i++) {
		this->fillPage(i, data);
	}
}

/**
 * Clear the LCD screen.
 */
void ERM19264UC1609Module::clear() {
	this->fillScreen(0x00);
}

/**
 * Fill the LCD screen.
 */
void ERM19264UC1609Module::fill() {
	this->fillScreen(0xFF);
}

/**
 * Draw an image.
 * @param x Image start point x.
 * @param y Image start point y.
 * @param width Image width.
 * @param height Image height.
 * @param data A "const PROGMEM" byte array data of image.
 */
void ERM19264UC1609Module::drawImage(byte x, byte y, byte width, byte height, const byte* data) {
	byte column = x % this->columns;
	byte page = y >> 3;
	for (byte dy = 0; dy < height; dy += 8) {
		if (y + dy < this->pages * 8) {
			this->setColumnAddress(column);
			this->setPageAddress(page++);
			for (byte dx = 0; dx < width; dx++) {
				if (x + dx < this->columns) {
					byte offset = (width * (dy >> 3)) + dx;
					this->sendData(pgm_read_byte(&data[offset]));
				}
			}
		}
	}
}

/**
 * Draw a character.
 * @param field x = field * font width.
 * @param line y = line * font height.
 * @param data between 0x20 to 0x7E.
 * @param font Font name.
 */
void ERM19264UC1609Module::drawText(byte field, byte line, char data, String font) {
	if (' ' <= data && data < 0x7F) {
		if (font == ASCII_UNIFONT_NAME) {
			byte x = field * ASCII_UNIFONT_WIDTH;
			byte y = line * ASCII_UNIFONT_HEIGHT;
			this->drawImage(x, y, ASCII_UNIFONT_WIDTH, ASCII_UNIFONT_HEIGHT, ASCII_UNIFONT_DATA[data - ' ']);
		} else {
			byte x = field * ASCII_8X6_WIDTH;
			byte y = line * ASCII_8X6_HEIGHT;
			this->drawImage(x, y, ASCII_8X6_WIDTH, ASCII_8X6_HEIGHT, ASCII_8X6_DATA[data - ' ']);
		}
	}
}

/**
 * Draw a string on LCD screen at field and line.
 * @param field x = field * font width.
 * @param line y = line * font height.
 * @param data A string.
 * @param font Font name.
 * @param autoNewLine When the cursor meet EOL: 0 = no auto new line; 1 = auto new line.
 */
void ERM19264UC1609Module::drawText(byte field, byte line, String data, String font, bool autoNewLine) {
	unsigned int length = data.length();
	for (unsigned int i = 0; i < length; i++) {
		if (autoNewLine) {
			byte width = field + 1;
			byte height = line + 1;
			if (font == ASCII_UNIFONT_NAME) {
				width *= ASCII_UNIFONT_WIDTH;
				height *= ASCII_UNIFONT_HEIGHT;
			} else {
				width *= ASCII_8X6_WIDTH;
				height *= ASCII_8X6_HEIGHT;
			}
			if (width > this->columns) {
				field = 0;
				line++;
			}
			if (height / 8 > this->pages) {
				line = 0;
			}
		}
		char c = data.charAt(i);
		if (c == '\n') {
			line++;
		} else if (c == '\r') {
			field = 0;
		} else if (c == '\t') {
			for (byte i = 0; i < ERM19264UC1609Module::SPACE_COUNT; i++) {
				this->drawText(field++, line, ' ', font);
			}
		} else {
			this->drawText(field++, line, c, font);
		}
	}
}

/**
 * Draw an character array on LCD screen at field and line.
 * @param field x = field * font width.
 * @param line y = line * font height.
 * @param data An character array.
 * @param font Font name.
 * @param autoNewLine When the cursor meet EOL: 0 = no auto new line; 1 = auto new line.
 */
void ERM19264UC1609Module::drawText(byte field, byte line, char* data, String font, bool autoNewLine) {
	this->drawText(field, line, String(data), font, autoNewLine);
}